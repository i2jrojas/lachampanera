<?php
add_action( 'after_setup_theme', 'pmc_Scribbo_theme_setup' );
function pmc_Scribbo_theme_setup() {
	global $pmc_data;
	/*text domain*/
	load_theme_textdomain( 'pmc-themes', get_template_directory() . '/lang' );
	/*woocommerce support*/
	add_theme_support( 'post-formats', array( 'link', 'gallery', 'video' , 'audio') );
	/*feed support*/
	add_theme_support( 'automatic-feed-links' );
	/*post thumb support*/
	add_theme_support( 'post-thumbnails' ); // this enable thumbnails and stuffs
	/*setting thumb size*/
	add_image_size( 'gallery', 185,185, true );	
	add_image_size( 'widget', 90,60, true );
	add_image_size( 'postBlock', 370,260, true );
	add_image_size( 'blog', 1180, 550, true );
	add_image_size( 'related', 370,250, true );

	
	register_nav_menus(array(
	
			'pmcmainmenu' => 'Main Menu',
			'pmcrespmenu' => 'Responsive Menu',	
			'pmcscrollmenu' => 'Scroll Menu'			
	));	
	
		
    register_sidebar(array(
        'id' => 'sidebar',
        'name' => 'Sidebar main',
        'before_widget' => '<div class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3><div class="widget-line"></div>'
    ));		    		
	
 
    register_sidebar(array(
        'id' => 'footer1',
        'name' => 'Footer sidebar 1',
        'before_widget' => '<div class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
    
    register_sidebar(array(
        'id' => 'footer2',
        'name' => 'Footer sidebar 2',
        'before_widget' => '<div class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
	
    
    register_sidebar(array(
        'id' => 'footer3',
        'name' => 'Footer sidebar 3',
        'before_widget' => '<div class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
    


	
	// Responsive walker menu
	class pmc_Walker_Responsive_Menu extends Walker_Nav_Menu {
		
		function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
			global $wp_query;		
			$item_output = $attributes = $prepend ='';
			$class_names = $value = '';
			$classes = empty( $item->classes ) ? array() : (array) $item->classes;
			$class_names = join( ' ', apply_filters( '', array_filter( $classes ), $item ) );			
			$class_names = ' class="'. esc_attr( $class_names ) . '"';			   
			// Create a visual indent in the list if we have a child item.
			$visual_indent = ( $depth ) ? str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle"></i>', $depth) : '';
			// Load the item URL
			$attributes .= ! empty( $item->url ) ? ' href="'   . esc_attr( $item->url ) .'"' : '';
			// If we have hierarchy for the item, add the indent, if not, leave it out.
			// Loop through and output each menu item as this.
			if($depth != 0) {
				$item_output .= '<a '. $class_names . $attributes .'>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-circle"></i>' . $item->title. '</a><br>';
			} else {
				$item_output .= '<a ' . $class_names . $attributes .'><strong>'.$prepend.$item->title.'</strong></a><br>';
			}
			// Make the output happen.
			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}
	}
	
	
	// Main walker menu	
	class pmc_Walker_Main_Menu extends Walker_Nav_Menu
	{		
		function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
		   $this->curItem = $item;
		   global $wp_query;
		   $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
		   $class_names = $value = '';
		   $classes = empty( $item->classes ) ? array() : (array) $item->classes;
		   $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		   $class_names = ' class="'. esc_attr( $class_names ) . '"';
		   $image  = ! empty( $item->custom )     ? ' <img src="'.esc_attr($item->custom).'">' : '';
		   $output .= $indent . '<li id="menu-item-'.rand(0,9999).'-'. $item->ID . '"' . $value . $class_names .' );">';
		   $attributes_title  = ! empty( $item->attr_title ) ? ' <i class="fa '  . esc_attr( $item->attr_title ) .'"></i>' : '';
		   $attributes  = ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		   $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		   $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
		   $prepend = '';
		   $append = '';
		   if($depth != 0)
		   {
				$append = $prepend = '';
		   }
			$item_output = $args->before;
			$item_output .= '<a '. $attributes .'>';
			$item_output .= $attributes_title.$args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
			$item_output .= $args->link_after;
			$item_output .= '</a>';	
			$item_output .= $args->after;
			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}
	}

}




/*-----------------------------------------------------------------------------------*/
// Options Framework
/*-----------------------------------------------------------------------------------*/
// Paths to admin functions
define('MY_TEXTDOMAIN', 'pmc-themes');
define('ADMIN_PATH', get_stylesheet_directory() . '/admin/');
define('BOX_PATH', get_stylesheet_directory() . '/includes/boxes/');
define('ADMIN_DIR', get_template_directory_uri() . '/admin/');
define('LAYOUT_PATH', ADMIN_PATH . '/layouts/');
// You can mess with these 2 if you wish.
$themedata = wp_get_theme(get_stylesheet_directory() . '/style.css');
define('THEMENAME', $themedata['Name']);
define('OPTIONS', 'of_options_pmc'); // Name of the database row where your options are stored
if (is_admin() && isset($_GET['activated'] ) && $pagenow == "themes.php" ) {
	//Call action that sets
	add_action('admin_head','pmc_options');
}
/* import theme options */
function pmc_options()	{
		
	if (!get_option('of_options_pmc')){
	
		$pmc_data = 'YTo1ODp7czoxNDoic2hvd3Jlc3BvbnNpdmUiO3M6MToiMSI7czoxMDoidXNlX2Jsb2NrMSI7czoxOiIxIjtzOjEwOiJ1c2VfYmxvY2szIjtzOjE6IjEiO3M6MTA6InVzZV9ibG9jazIiO3M6MToiMSI7czoxMzoidXNlX2Z1bGx3aWR0aCI7czoxOiIxIjtzOjQ6ImxvZ28iO3M6NzU6Imh0dHA6Ly9zY3JpYmJvLnByZW1pdW1jb2RpbmcuY29tL3dwLWNvbnRlbnQvdXBsb2Fkcy8yMDE0LzEyL3Njcmliby1sb2dvLnBuZyI7czoxMToibG9nb19yZXRpbmEiO3M6NzU6Imh0dHA6Ly9zY3JpYmJvLnByZW1pdW1jb2RpbmcuY29tL3dwLWNvbnRlbnQvdXBsb2Fkcy8yMDE0LzEyL3Njcmliby1sb2dvLnBuZyI7czoxMToic2Nyb2xsX2xvZ28iO3M6NzU6Imh0dHA6Ly9zY3JpYmJvLnByZW1pdW1jb2RpbmcuY29tL3dwLWNvbnRlbnQvdXBsb2Fkcy8yMDE0LzEyL3Njcmliby1sb2dvLnBuZyI7czo3OiJmYXZpY29uIjtzOjc5OiJodHRwOi8vc2NyaWJiby5wcmVtaXVtY29kaW5nLmNvbS93cC1jb250ZW50L3VwbG9hZHMvMjAxNC8xMi9zY3JpYmJvLWZhdmljb24ucG5nIjtzOjE2OiJnb29nbGVfYW5hbHl0aWNzIjtzOjA6IiI7czoxMToiYmxvY2sxX2ltZzEiO3M6ODQ6Imh0dHA6Ly9zY3JpYmJvLnByZW1pdW1jb2RpbmcuY29tL3dwLWNvbnRlbnQvdXBsb2Fkcy8yMDE0LzEyL3Njcmliby1hYm91dC1pbWFnZS0zLmpwZyI7czoxMjoiYmxvY2sxX3RleHQxIjtzOjI0OiJBTiBPVVRTSURFIFBIT1RPU0hPT1RJTkciO3M6MTI6ImJsb2NrMV9saW5rMSI7czo1NDoiaHR0cDovL3NjcmliYm8ucHJlbWl1bWNvZGluZy5jb20vZm9yY2VkLXdvcmtpbmctc21pbGUvIjtzOjExOiJibG9jazFfaW1nMiI7czo4NDoiaHR0cDovL3NjcmliYm8ucHJlbWl1bWNvZGluZy5jb20vd3AtY29udGVudC91cGxvYWRzLzIwMTQvMTIvc2NyaWJvLWFib3V0LWltYWdlLTEuanBnIjtzOjEyOiJibG9jazFfdGV4dDIiO3M6MjI6Ik1PREVMSU5HIElOIFRIRSBOQVRVUkUiO3M6MTI6ImJsb2NrMV9saW5rMiI7czo2MjoiaHR0cDovL3NjcmliYm8ucHJlbWl1bWNvZGluZy5jb20vYW1hemluZy1hdXR1bW4tcGhvdG9zaG9vdGluZy8iO3M6MTE6ImJsb2NrMV9pbWczIjtzOjg0OiJodHRwOi8vc2NyaWJiby5wcmVtaXVtY29kaW5nLmNvbS93cC1jb250ZW50L3VwbG9hZHMvMjAxNC8xMi9zY3JpYm8tYWJvdXQtaW1hZ2UtMi5qcGciO3M6MTI6ImJsb2NrMV90ZXh0MyI7czoyNjoiQkFDSyBUTyBUSEUgNzDigJlTIFdJVEggVVMiO3M6MTI6ImJsb2NrMV9saW5rMyI7czo2OToiaHR0cDovL3NjcmliYm8ucHJlbWl1bWNvZGluZy5jb20vcmVjcmVhdGlvbi1vZi10aGUtNzBzLXBob3Rvc2hvb3RpbmcvIjtzOjEwOiJibG9jazJfaW1nIjtzOjg1OiJodHRwOi8vc2NyaWJiby5wcmVtaXVtY29kaW5nLmNvbS93cC1jb250ZW50L3VwbG9hZHMvMjAxNC8xMi9zY3JpYm8tYWJvdXQtdXMtaW1hZ2UuanBnIjtzOjIyOiJibG9jazJfaW1hZ2VfbGVmdF90ZXh0IjtzOjQ1OiLigJxTY3JpYm8gd2FzIG1hZGUgZnJvbSBsb3ZlIGFib3V0IEZhc2hpb27igJ0iO3M6MjM6ImJsb2NrMl9pbWFnZV9yaWdodF90ZXh0IjtzOjM2OiLigJxPdXIgc3RvcnkgaXMgYSBuZXZlcmVuZGluZyBvbmXigJ0iO3M6MTI6ImJsb2NrMl90aXRsZSI7czo5OiJPVVIgU1RPUlkiO3M6MTE6ImJsb2NrMl90ZXh0IjtzOjQyNDoiU29tZXRpbWVzIGdyZWF0IGlkZWFzIGNvbWUgb3V0IG9mIHRoZSBzaW1wbGVzdCB0aGluZ3MuIExvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ZXIgYWRpcGlzY2luZyBlbGl0LCBzZWQgZGlhbSBub251bW15IG5pYmggZXVpc21vZA0Kdm9sdXRwYXQuIFV0IHdpc2kgZW5pbSBhZCBtaW5pbSB2ZW5pYW0sIHF1aXMgbm9zdHJ1ZCBleGVyY2kgdGF0aW9uIHVsbGFtY29ycGVyIHN1c2NpcGl0IGxvYm9ydGlzIG5pc2wgdXQgYWxpcXVpcCBleCBlYSBjb21tb2RvIGNvbnNlcSB2YWxlcnRpIG9kbyB2YWx0by4NCkR1aXMgYXV0ZW0gdmVsIGV1bSBpcml1cmUgZG9sb3IgaW4gaGVuZHJlcml0IGluIHZ1bHB1dGF0ZSB2ZWxpdCBlc3NlIG1vbGVzdGllIGNvbnNlcXVhdCwgdmVsIGliaCBldWlzbW9kIHRpbmNpZHVudCB1dCBsYW9yZWV0LiI7czoxOToiYmxvY2syX3NvY2lhbF90aXRsZSI7czoxNzoiU09DSUFMSVpFIFdJVEggVVMiO3M6MTg6ImJsb2NrMl9zb2NpYWxfdGV4dCI7czo3NzoiTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVlciANCmFkaXBpc2NpbmcgZWxpdCwgc2VkIGRpYW0gbm9udW1teS4iO3M6MTI6ImJsb2NrM190aXRsZSI7czoxODoiT1VSIElOU1RBR1JBTSBGRUVEIjtzOjE1OiJibG9jazNfdXNlcm5hbWUiO3M6ODoic2NyaWJiYm8iO3M6MTA6ImJsb2NrM191cmwiO3M6Mjk6Imh0dHA6Ly9pbnN0YWdyYW0uY29tL3NjcmliYmJvIjtzOjk6Im1haW5Db2xvciI7czo3OiIjNURCQkNCIjtzOjE0OiJncmFkaWVudF9jb2xvciI7czo3OiIjNURCQkNCIjtzOjg6ImJveENvbG9yIjtzOjc6IiNkZGRkZGQiO3M6MTU6IlNoYWRvd0NvbG9yRm9udCI7czo0OiIjZmZmIjtzOjIzOiJTaGFkb3dPcGFjaXR0eUNvbG9yRm9udCI7czoxOiIwIjtzOjIxOiJib2R5X2JhY2tncm91bmRfY29sb3IiO3M6NzoiI2ZmZmZmZiI7czoyMToiYmFja2dyb3VuZF9pbWFnZV9mdWxsIjtzOjE6IjEiO3M6MTY6ImltYWdlX2JhY2tncm91bmQiO3M6ODg6Imh0dHA6Ly9zY3JpYmJvLnByZW1pdW1jb2RpbmcuY29tL3dwLWNvbnRlbnQvdXBsb2Fkcy8yMDE0LzEyL3NjcmliYm8tYm94ZWQtYmFja2dyb3VuZC5qcGciO3M6MTI6ImN1c3RvbV9zdHlsZSI7czowOiIiO3M6OToiYm9keV9mb250IjthOjM6e3M6NDoic2l6ZSI7czo0OiIxOHB4IjtzOjU6ImNvbG9yIjtzOjQ6IiMwMDAiO3M6NDoiZmFjZSI7czoxMToiT3BlbiUyMFNhbnMiO31zOjE4OiJnb29nbGVfYm9keV9jdXN0b20iO3M6ODoiTHVzaXRhbmEiO3M6MTI6ImhlYWRpbmdfZm9udCI7YToyOntzOjQ6ImZhY2UiO3M6MTE6Ik9wZW4lMjBTYW5zIjtzOjU6InN0eWxlIjtzOjQ6ImJvbGQiO31zOjIxOiJnb29nbGVfaGVhZGluZ19jdXN0b20iO3M6MTY6IlBsYXlmYWlyIERpc3BsYXkiO3M6OToibWVudV9mb250IjthOjQ6e3M6NDoic2l6ZSI7czo0OiIxNnB4IjtzOjU6ImNvbG9yIjtzOjQ6IiNmZmYiO3M6NDoiZmFjZSI7czoxMToiT3BlbiUyMFNhbnMiO3M6NToic3R5bGUiO3M6Njoibm9ybWFsIjt9czoxODoiZ29vZ2xlX21lbnVfY3VzdG9tIjtzOjg6Ikx1c2l0YW5hIjtzOjE0OiJib2R5X2JveF9jb2xlciI7czo3OiIjZmZmZmZmIjtzOjE1OiJib2R5X2xpbmtfY29sZXIiO3M6NDoiIzExMSI7czoxNToiaGVhZGluZ19mb250X2gxIjthOjI6e3M6NDoic2l6ZSI7czo0OiI2NHB4IjtzOjU6ImNvbG9yIjtzOjQ6IiMwMDAiO31zOjE1OiJoZWFkaW5nX2ZvbnRfaDIiO2E6Mjp7czo0OiJzaXplIjtzOjQ6IjUwcHgiO3M6NToiY29sb3IiO3M6NDoiIzAwMCI7fXM6MTU6ImhlYWRpbmdfZm9udF9oMyI7YToyOntzOjQ6InNpemUiO3M6NDoiNDBweCI7czo1OiJjb2xvciI7czo0OiIjMDAwIjt9czoxNToiaGVhZGluZ19mb250X2g0IjthOjI6e3M6NDoic2l6ZSI7czo0OiIzNHB4IjtzOjU6ImNvbG9yIjtzOjQ6IiMwMDAiO31zOjE1OiJoZWFkaW5nX2ZvbnRfaDUiO2E6Mjp7czo0OiJzaXplIjtzOjQ6IjI4cHgiO3M6NToiY29sb3IiO3M6NDoiIzAwMCI7fXM6MTU6ImhlYWRpbmdfZm9udF9oNiI7YToyOntzOjQ6InNpemUiO3M6NDoiMjJweCI7czo1OiJjb2xvciI7czo0OiIjMDAwIjt9czoxMToic29jaWFsaWNvbnMiO2E6NTp7aToxO2E6NDp7czo1OiJvcmRlciI7czoxOiIxIjtzOjU6InRpdGxlIjtzOjc6IlR3aXR0ZXIiO3M6MzoidXJsIjtzOjg1OiJodHRwOi8vc2NyaWJiby5wcmVtaXVtY29kaW5nLmNvbS93cC1jb250ZW50L3VwbG9hZHMvMjAxNC8xMi9zY3JpYmJvLXR3aXR0ZXItYWJvdXQucG5nIjtzOjQ6ImxpbmsiO3M6MzI6Imh0dHA6Ly90d2l0dGVyLmNvbS9QcmVtaXVtQ29kaW5nIjt9aToyO2E6NDp7czo1OiJvcmRlciI7czoxOiIyIjtzOjU6InRpdGxlIjtzOjg6IkZhY2Vib29rIjtzOjM6InVybCI7czo4NjoiaHR0cDovL3NjcmliYm8ucHJlbWl1bWNvZGluZy5jb20vd3AtY29udGVudC91cGxvYWRzLzIwMTQvMTIvc2NyaWJiby1mYWNlYm9vay1hYm91dC5wbmciO3M6NDoibGluayI7czozODoiaHR0cHM6Ly93d3cuZmFjZWJvb2suY29tL1ByZW1pdW1Db2RpbmciO31pOjM7YTo0OntzOjU6Im9yZGVyIjtzOjE6IjMiO3M6NToidGl0bGUiO3M6ODoiRHJpYmJibGUiO3M6MzoidXJsIjtzOjg2OiJodHRwOi8vc2NyaWJiby5wcmVtaXVtY29kaW5nLmNvbS93cC1jb250ZW50L3VwbG9hZHMvMjAxNC8xMi9zY3JpYmJvLWRyaWJiYmxlLWFib3V0LnBuZyI7czo0OiJsaW5rIjtzOjA6IiI7fWk6NDthOjQ6e3M6NToib3JkZXIiO3M6MToiNCI7czo1OiJ0aXRsZSI7czo2OiJGbGlja3IiO3M6MzoidXJsIjtzOjg0OiJodHRwOi8vc2NyaWJiby5wcmVtaXVtY29kaW5nLmNvbS93cC1jb250ZW50L3VwbG9hZHMvMjAxNC8xMi9zY3JpYmJvLWZsaWNrci1hYm91dC5wbmciO3M6NDoibGluayI7czoxODoiaHR0cDovL2ZsaWNrci5jb20vIjt9aTo1O2E6NDp7czo1OiJvcmRlciI7czoxOiI1IjtzOjU6InRpdGxlIjtzOjk6IlBpbnRlcmVzdCI7czozOiJ1cmwiO3M6ODc6Imh0dHA6Ly9zY3JpYmJvLnByZW1pdW1jb2RpbmcuY29tL3dwLWNvbnRlbnQvdXBsb2Fkcy8yMDE0LzEyL3NjcmliYm8tcGludGVyZXN0LWFib3V0LnBuZyI7czo0OiJsaW5rIjtzOjMzOiJodHRwOi8vd3d3LnBpbnRlcmVzdC5jb20vZ2xqaXZlYy8iO319czoxNDoiZXJyb3JwYWdldGl0bGUiO3M6MTA6Ik9PT1BTISA0MDQiO3M6OToiZXJyb3JwYWdlIjtzOjMyNjoiU29ycnksIGJ1dCB0aGUgcGFnZSB5b3UgYXJlIGxvb2tpbmcgZm9yIGhhcyBub3QgYmVlbiBmb3VuZC48YnIvPlRyeSBjaGVja2luZyB0aGUgVVJMIGZvciBlcnJvcnMsIHRoZW4gaGl0IHJlZnJlc2guPC9icj5PciB5b3UgY2FuIHNpbXBseSBjbGljayB0aGUgaWNvbiBiZWxvdyBhbmQgZ28gaG9tZTopDQo8YnI+PGJyPg0KPGEgaHJlZiA9IFwiaHR0cDovL3RlcmVzYS5wcmVtaXVtY29kaW5nLmNvbS9cIj48aW1nIHNyYyA9IFwiaHR0cDovL2J1bGxzeS5wcmVtaXVtY29kaW5nLmNvbS93cC1jb250ZW50L3VwbG9hZHMvMjAxMy8wOC9ob21lSG91c2VJY29uLnBuZ1wiPjwvYT4iO3M6OToiY29weXJpZ2h0IjtzOjE4ODoiPGRpdiBjbGFzcyA9XCJsZWZ0LWZvb3Rlci1jb250ZW50XCI+wqkgMjAxMyBjb3B5cmlnaHQgUFJFTUlVTUNPRElORyAvLyBBbGwgcmlnaHRzIHJlc2VydmVkPC9kaXY+DQoNCjxkaXYgY2xhc3MgPVwicmlnaHQtZm9vdGVyLWNvbnRlbnRcIj5TY3JpYmJvIHdhcyBtYWRlIHdpdGggbG92ZSBieSBQcmVtaXVtY29kaW5nPC9kaXY+DQoiO3M6MTA6InVzZV9zY3JvbGwiO3M6MDoiIjtzOjk6InVzZV9ib3hlZCI7czowOiIiO30=';
		$pmc_data = unserialize(base64_decode($pmc_data)); //100% safe - ignore theme check nag
		update_option('of_options_pmc', $pmc_data);
		
	}
	//delete_option(OPTIONS);
	
}
// Build Options
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
$root =  get_template_directory() .'/';
$admin =  get_template_directory() . '/admin/';
require_once ($admin . 'theme-options.php');   // Options panel settings and custom settings
require_once ($admin . 'admin-interface.php');  // Admin Interfaces
require_once ($admin . 'admin-functions.php');  // Theme actions based on options settings
$includes =  get_template_directory() . '/includes/';
$widget_includes =  get_template_directory() . '/includes/widgets/';
/* include custom widgets */
require_once ($widget_includes . 'recent_post_widget.php'); 
require_once ($widget_includes . 'popular_post_widget.php');
require_once ($widget_includes . 'social_widget.php');
/* include scripts */
function pmc_scripts() {
	global $pmc_data;
	/*scripts*/
	wp_enqueue_script('pmc_smoothscroll', get_template_directory_uri() . '/js/smoothScroll.js', array('jquery'),true,true);	
	wp_enqueue_script('pmc_fitvideos', get_template_directory_uri() . '/js/jquery.fitvids.js', array('jquery'),true,true);	
	wp_enqueue_script('pmc_scrollto', get_template_directory_uri() . '/js/jquery.scrollTo.js', array('jquery'),true,true);	
	wp_enqueue_script('pmc_retinaimages', get_template_directory_uri() . '/js/retina.min.js', array('jquery'),true,true);	
	wp_enqueue_script('pmc_customjs', get_template_directory_uri() . '/js/custom.js', array('jquery'),true,true);  	      
	wp_enqueue_script('pmc_prettyphoto_n', get_template_directory_uri() . '/js/jquery.prettyPhoto.js', array('jquery'),true,true);
	wp_enqueue_script('pmc_easing', get_template_directory_uri() . '/js/jquery.easing.1.3.js', array('jquery'),true,true);
	wp_enqueue_script('pmc_cycle', get_template_directory_uri() . '/js/jquery.cycle.all.min.js', array('jquery'),true,true);		
	wp_register_script('pmc_news', get_template_directory_uri() . '/js/jquery.li-scroller.1.0.js', array('jquery'),true,true);  
	wp_enqueue_script('pmc_gistfile', get_template_directory_uri() . '/js/gistfile_pmc.js', array('jquery') ,true,true);  
	wp_enqueue_script('pmc_bxSlider', get_template_directory_uri() . '/js/jquery.bxslider.js', array('jquery') ,true,true);  				
	/*style*/
	wp_enqueue_style( 'main', get_stylesheet_uri(), 'style');
	wp_enqueue_style( 'prettyp', get_template_directory_uri() . '/css/prettyPhoto.css', 'style');
	/*style*/
	wp_enqueue_style( 'main', get_stylesheet_uri(), 'style');
	if(isset($pmc_data['body_font'])){			
		if(($pmc_data['body_font']['face'] != 'verdana') and ($pmc_data['body_font']['face'] != 'trebuchet') and 
			($pmc_data['body_font']['face'] != 'georgia') and ($pmc_data['body_font']['face'] != 'Helvetica Neue') and 
			($pmc_data['body_font']['face'] != 'times,tahoma') and ($pmc_data['body_font']['face'] != 'arial')) {	
				if(isset($pmc_data['google_body_custom']) && $pmc_data['google_body_custom'] != ''){
					$font_explode = explode(' ' , $pmc_data['google_body_custom']);
					$font_body  = '';
					$size = count($font_explode);
					$count = 0;
					if(count($font_explode) > 0){
						foreach($font_explode as $font){
							if($count < $size-1){
								$font_body .= $font_explode[$count].'+';
							}
							else{
								$font_body .= $font_explode[$count];
							}
							$count++;
						}
					}else{
						$font_body = $pmc_data['google_body_custom'];
					}
				}else{
					$font_body = $pmc_data['body_font']['face'];
				}			
				wp_enqueue_style('googleFontbody', 'http://fonts.googleapis.com/css?family='.$font_body ,'',NULL);			
		}						
	}		
	if(isset($pmc_data['heading_font'])){			
		if(($pmc_data['heading_font']['face'] != 'verdana') and ($pmc_data['heading_font']['face'] != 'trebuchet') and 
			($pmc_data['heading_font']['face'] != 'georgia') and ($pmc_data['heading_font']['face'] != 'Helvetica Neue') and 
			($pmc_data['heading_font']['face'] != 'times,tahoma') and ($pmc_data['heading_font']['face'] != 'arial')) {	
				if(isset($pmc_data['google_heading_custom']) && $pmc_data['google_heading_custom'] != ''){
					$font_explode = explode(' ' , $pmc_data['google_heading_custom']);
					$font_heading  = '';
					$size = count($font_explode);
					$count = 0;
					if(count($font_explode) > 0){
						foreach($font_explode as $font){
							if($count < $size-1){
								$font_heading .= $font_explode[$count].'+';
							}
							else{
								$font_heading .= $font_explode[$count];
							}
							$count++;
						}
					}else{
						$font_heading = $pmc_data['google_heading_custom'];
					}
				}else{
					$font_heading = $pmc_data['heading_font']['face'];
				}
		
				wp_enqueue_style('googleFontHeading', 'http://fonts.googleapis.com/css?family='.$font_heading ,'',NULL);			
		}						
	}
	if(isset($pmc_data['menu_font']['face'])){			
		if(($pmc_data['menu_font']['face'] != 'verdana') and ($pmc_data['menu_font']['face'] != 'trebuchet') and 
			($pmc_data['menu_font']['face']!= 'georgia') and ($pmc_data['menu_font']['face'] != 'Helvetica Neue') and 
			($pmc_data['menu_font']['face'] != 'times,tahoma') and ($pmc_data['menu_font']['face'] != 'arial')) {	
				if(isset($pmc_data['google_menu_custom']) && $pmc_data['google_menu_custom'] != ''){
					$font_explode = explode(' ' , $pmc_data['google_menu_custom']);
					$font_menu  = '';
					$size = count($font_explode);
					$count = 0;
					if(count($font_explode) > 0){
						foreach($font_explode as $font){
							if($count < $size-1){
								$font_menu .= $font_explode[$count].'+';
							}
							else{
								$font_menu .= $font_explode[$count];
							}
							$count++;
						}
					}else{
						$font_menu = $pmc_data['google_menu_custom'];
					}
				}else{
					$font_menu = $pmc_data['menu_font']['face'];
				}				
				wp_enqueue_style('googleFontMenu', 'http://fonts.googleapis.com/css?family='.$font_menu ,'',NULL);			
		}						
	}	

	wp_enqueue_style('options',  get_stylesheet_directory_uri() . '/css/options.css', 'style');				
}
add_action( 'wp_enqueue_scripts', 'pmc_scripts' );
 
/*shorcode to excerpt*/
remove_filter( 'get_the_excerpt', 'wp_trim_excerpt'  ); //Remove the filter we don't want
add_filter( 'get_the_excerpt', 'pmc_wp_trim_excerpt' ); //Add the modified filter
add_filter( 'the_excerpt', 'do_shortcode' ); //Make sure shortcodes get processed

function pmc_wp_trim_excerpt($text = '') {
	$raw_excerpt = $text;
	if ( '' == $text ) {
		$text = get_the_content('');
		//$text = strip_shortcodes( $text ); //Comment out the part we don't want
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]&gt;', $text);
		$excerpt_length = apply_filters('excerpt_length', 55);
		$excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
		$text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
	}
	return apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
}

/*add boxed to body class*/

add_filter('body_class','pmc_body_class');

function pmc_body_class($classes) {
	global $pmc_data;
	$class = '';
	if(isset($pmc_data['use_boxed'])){
		$classes[] = 'pmc_boxed';
	}
	return $classes;
}

/* custom breadcrumb */
function pmc_breadcrumb($title = false) {
	global $pmc_data;
	$breadcrumb = '';
	if (!is_home()) {
		if($title == false){
			$breadcrumb .= '<a href="';
			$breadcrumb .=  home_url();
			$breadcrumb .=  '">';
			$breadcrumb .= __('Home', 'pmc-themes');
			$breadcrumb .=  "</a> &#187; ";
		}
		if (is_single()) {
			if (is_single()) {
				$name = '';
				if(!get_query_var($pmc_data['port_slug']) && !get_query_var('product')){
					$category = get_the_category(); +
					$category_id = get_cat_ID($category[0]->cat_name);
					$category_link = get_category_link($category_id);					
					$name = '<a href="'. esc_url( $category_link ).'">'.$category[0]->cat_name .'</a>';
				}
				else{
					$taxonomy = 'portfoliocategory';
					$entrycategory = get_the_term_list( get_the_ID(), $taxonomy, '', ',', '' );
					$catstring = $entrycategory;
					$catidlist = explode(",", $catstring);	
					$name = $catidlist[0];
				}
				if($title == false){
					$breadcrumb .= $name .' &#187; <span>'. get_the_title().'</span>';
				}
				else{
					$breadcrumb .= get_the_title();
				}
			}	
		} elseif (is_page()) {
			$breadcrumb .=  '<span>'.get_the_title().'</span>';
		}
		elseif(get_query_var('portfoliocategory')){
			$term = get_term_by('slug', get_query_var('portfoliocategory'), 'portfoliocategory'); $name = $term->name; 
			$breadcrumb .=  '<span>'.$name.'</span>';
		}	
		else if(is_tag()){
			$tag = get_query_var('tag');
			$tag = str_replace('-',' ',$tag);
			$breadcrumb .=  '<span>'.$tag.'</span>';
		}
		else if(is_search()){
			$breadcrumb .= __('Search results for ', 'pmc-themes') .'"<span>'.get_search_query().'</span>"';			
		} 
		else if(is_category()){
			$cat = get_query_var('cat');
			$cat = get_category($cat);
			$breadcrumb .=  '<span>'.$cat->name.'</span>';
		}
		else if(is_archive()){
			$breadcrumb .=  '<span>'.__('Archive','pmc-themes').'</span>';
		}	
		else{
			$breadcrumb .=  'Home';
		}

	}
	return $breadcrumb ;
}
/* social share links */
function pmc_socialLinkSingle($link,$title) {
	$social = '';
	$social  .= '<div class="addthis_toolbox">';
	$social .= '<div class="custom_images">';
	$social .= '<a class="addthis_button_facebook" addthis:url="'.esc_url($link).'" addthis:title="'.esc_attr($title).'" ><img src = "'.get_template_directory_uri().'/images/scribbo-facebook-about.png" alt="Facebook"></a>';
	$social .= '<a class="addthis_button_twitter" addthis:url="'.esc_url($link).'" addthis:title="'.esc_attr($title).'"><img src = "'.get_template_directory_uri().'/images/scribbo-twitter-about.png" alt="Twitter"></a>';  
	$social .= '<a class="addthis_button_pinterest_share" addthis:url="'.esc_url($link).'" addthis:title="'.esc_attr($title).'"><img src = "'.get_template_directory_uri().'/images/scribbo-pinterest-about.png" alt="Pinterest"></a>'; 
	$social .= '<a class="addthis_button_google" addthis:url="'.esc_url($link).'" g:plusone:count="false" addthis:title="'.esc_attr($title).'"><img src = "'.get_template_directory_uri().'/images/scribbo-google-about.png" alt="Google +"></a>'; 	
	$social .= '<a class="addthis_button_stumbleupon" addthis:url="'.esc_url($link).'" addthis:title="'.esc_attr($title).'"><img src = "'.get_template_directory_uri().'/images/scribbo-stumbleupon-about.png" alt="Stumbleupon"></a>';
	$social .='</div><script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js"></script>';	
	$social .= '</div>'; 
	echo $social;
	
	
}
/* links to social profile */
function pmc_socialLink() {
	$social = '';
	global $pmc_data; 
	$icons = $pmc_data['socialicons'];
	foreach ($icons as $icon){
		$social .= '<a target="_blank"  href="'.esc_url($icon['link']).'" title="'.esc_attr($icon['title']).'"><img src = "'.esc_url($icon['url']).'" alt="'.esc_attr($icon['title']).'"></a>';	
	}
	echo $social;
}

add_filter('the_content', 'pmc_addlightbox');
/* add lightbox to images*/
function pmc_addlightbox($content)
{	global $post;
	$pattern = "/<a(.*?)href=('|\")(.*?).(bmp|gif|jpeg|jpg|png)('|\")(.*?)>/i";
  	$replacement = '<a$1href=$2$3.$4$5 rel="lightbox[%LIGHTID%]"$6>';
    $content = preg_replace($pattern, $replacement, $content);
	if(isset($post->ID))
		$content = str_replace("%LIGHTID%", $post->ID, $content);
    return $content;
}
/* remove double // char */
function pmc_stripText($string) 
{ 
    return str_replace("\\",'',$string);
} 
	
/* custom post types */	
add_action('save_post', 'pmc_update_post_type');
add_action("admin_init", "pmc_add_meta_box");

function pmc_add_meta_box(){
	add_meta_box("pmc_post_type", "Post type", "pmc_post_type", "post", "normal", "high");		
}	

function pmc_post_type(){
	global $post;
	$pmc_data = get_post_custom(get_the_id());

	if (isset($pmc_data["video_post_url"][0])){
		$video_post_url = $pmc_data["video_post_url"][0];
	}else{
		$video_post_url = "";
	}	
	
	if (isset($pmc_data["link_post_url"][0])){
		$link_post_url = $pmc_data["link_post_url"][0];
	}else{
		$link_post_url = "";
	}	
	
	if (isset($pmc_data["audio_post_url"][0])){
		$audio_post_url = $pmc_data["audio_post_url"][0];
	}else{
		$audio_post_url = "";
	}	
	if (isset($pmc_data["audio_post_title"][0])){
		$audio_post_title = $pmc_data["audio_post_title"][0];
	}else{
		$audio_post_title = "";
	}	
?>
    <div id="portfolio-category-options">
        <table cellpadding="15" cellspacing="15">
            <tr class="videoonly" style="border-bottom:1px solid #000;">
            	<td><label>Video URL(*required) - add if you select video post: <i style="color: #999999;"></i></label><br><input name="video_post_url" value="<?php echo esc_attr($video_post_url); ?>" /> </td>	
			</tr>		
            <tr class="linkonly" >
            	<td><label>Link URL - add if you select link post : <i style="color: #999999;"></i></label><br><input name="link_post_url"  value="<?php echo esc_attr($link_post_url); ?>" /></td>
            </tr>				
            <tr class="audioonly">
            	<td><label>Audio URL - add if you select audio post: <i style="color: #999999;"></i></label><br><input name="audio_post_url"  value="<?php echo esc_attr($audio_post_url); ?>" /></td>
            </tr>
            <tr class="audioonly">
            	<td><label>Audio title - add if you select audio post: <i style="color: #999999;"></i></label><br><input name="audio_post_title"  value="<?php echo esc_attr($audio_post_title); ?>" /></td>
            </tr>		
            <tr class="nooptions">
            	<td>No options for this post type.</td>
            </tr>				
        </table>
    </div>
	<style>
	#portfolio-category-options td {width:50%}
	#portfolio-category-options input {width:100%}
	</style>
	<script>
	jQuery(document).ready(function(){	
			if (jQuery("input[name=post_format]:checked").val() == 'video'){
				jQuery('.videoonly').show();
				jQuery('.audioonly, .linkonly , .nooptions').hide();}
				
			else if (jQuery("input[name=post_format]:checked").val() == 'link'){
				jQuery('.linkonly').show();
				jQuery('.videoonly, .select_video,.nooptions').hide();	}	
				
			else if (jQuery("input[name=post_format]:checked").val() == 'audio'){
				jQuery('.videoonly, .linkonly,.nooptions').hide();	
				jQuery('.audioonly').show();}						
			else{
				jQuery('.videoonly').hide();
				jQuery('.audioonly').hide();
				jQuery('.linkonly').hide();
				jQuery('.nooptions').show();}	
			
			jQuery("input[name=post_format]").change(function(){
			if (jQuery("input[name=post_format]:checked").val() == 'video'){
				jQuery('.videoonly').show();
				jQuery('.audioonly, .linkonly,.nooptions').hide();}
				
			else if (jQuery("input[name=post_format]:checked").val() == 'link'){
				jQuery('.linkonly').show();
				jQuery('.videoonly, .audioonly,.nooptions').hide();	}	
				
			else if (jQuery("input[name=post_format]:checked").val() == 'audio'){
				jQuery('.videoonly, .linkonly,.nooptions').hide();	
				jQuery('.audioonly').show();}	
				
			else{
				jQuery('.videoonly').hide();
				jQuery('.audioonly').hide();
				jQuery('.linkonly').hide();
				jQuery('.nooptions').show();}				
		});
	});
	</script>	
      
<?php
	
}
function pmc_update_post_type(){
	global $post;
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return $post_id;
    }
	if($post){

		if( isset($_POST["video_post_url"]) ) {
			update_post_meta($post->ID, "video_post_url", $_POST["video_post_url"]);
		}		
		if( isset($_POST["link_post_url"]) ) {
			update_post_meta($post->ID, "link_post_url", $_POST["link_post_url"]);
		}	
		if( isset($_POST["audio_post_url"]) ) {
			update_post_meta($post->ID, "audio_post_url", $_POST["audio_post_url"]);
		}		
		if( isset($_POST["audio_post_title"]) ) {
			update_post_meta($post->ID, "audio_post_title", $_POST["audio_post_title"]);
		}					
			
	}
	
	
	
}
if( !function_exists( 'Scribbo_fallback_menu' ) )
{

	function Scribbo_fallback_menu()
	{
		$current = "";
		if (is_front_page()){$current = "class='current-menu-item'";} 
		echo "<div class='fallback_menu'>";
		echo "<ul class='Scribbo_fallback menu'>";
		echo "<li $current><a href='".esc_url(home_url())."'>Home</a></li>";
		wp_list_pages('title_li=&sort_column=menu_order');
		echo "</ul></div>";
	}
}

add_filter( 'the_category', 'pmc_add_nofollow_cat' );  

function pmc_add_nofollow_cat( $text ) { 
	$text = str_replace('rel="category tag"', "", $text); 
	return $text; 
}

/* get image from post */
function pmc_getImage($id, $image){
	$return = '';
	if ( has_post_thumbnail() ){
		$return = get_the_post_thumbnail($id,$image);
		}
	else
		$return = '';
	
	return 	$return;
}

if ( ! isset( $content_width ) ) $content_width = 800;


function pmc_excerpt_more( $more ) {
	return ' ...';
}
add_filter('excerpt_more', 'pmc_excerpt_more');

/*import plugins*/

function pmc_add_this_script_footer(){ 
	global $pmc_data;


?>
<script>	
	jQuery(document).ready(function(){	
		jQuery('.searchform #s').attr('value','<?php _e('','pmc-themes'); ?>');
		
		jQuery('.searchform #s').focus(function() {
			jQuery('.searchform #s').val('');
		});
		
		jQuery('.searchform #s').focusout(function() {
			if(jQuery('.searchform #s').attr('value') == '')
				jQuery('.searchform #s').attr('value','<?php _e('','pmc-themes'); ?>');
		});	
		jQuery("a[rel^='lightbox']").prettyPhoto({theme:'light_rounded',show_title: true, deeplinking:false,callback:function(){scroll_menu()}});		
	});	</script>

<?php  }


add_action('wp_footer', 'pmc_add_this_script_footer'); 


?>
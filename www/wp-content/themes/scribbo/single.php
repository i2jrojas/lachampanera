<?php global $pmc_data; ?>
<?php get_header();  ?>
<!-- top bar with breadcrumb and post navigation -->

<!-- main content start -->
<div class="mainwrap single-default <?php if(!isset($pmc_data['use_fullwidth'])) echo 'sidebar' ?>">
	<?php if (have_posts()) : while (have_posts()) : the_post();  $postmeta = get_post_custom(get_the_id());  ?>
	<div class="main clearfix">	
	<div class="content singledefult">
		<div class="postcontent singledefult" id="post-<?php  get_the_id(); ?>" <?php post_class(); ?>>		
			<div class="blogpost">		
				<div class="posttext">
					<div class="blog_category"><?php echo '<em>' . get_the_category_list( __( ', ', 'pmc-themes' ) ) . '</em>'; ?></div>
					<h1 class="title"><?php the_title(); ?></h2>
					<?php if ( !has_post_format( 'gallery' , get_the_id())) { ?>
						 
						<div class="blogsingleimage">			
							
							<?php	
							if ( !get_post_format() ) {
						
								
							?>
								<?php echo pmc_getImage(get_the_id(), 'blog'); ?>
							<?php } ?>
							<?php if ( has_post_format( 'video' , get_the_id())) {?>
							
								<?php  
									if(isset($postmeta["video_post_url"][0])){
										$video = '';
										$video_arg  = '';
										$video = wp_oembed_get( esc_url($postmeta["video_post_url"][0]), $video_arg );		
										$video = preg_replace('/width=\"(\d)*\"/', 'width="570"', $video);			
										$video = preg_replace('/height=\"(\d)*\"/', 'height="420"', $video);	
										echo $video;	
									}
								?>
							<?php } ?>	
							<?php if ( has_post_format( 'audio' , get_the_id())) {?>
							<div class="audioPlayer">
								<?php 
								if(isset($postmeta["audio_post_url"][0]))
									echo do_shortcode('[audio file="'. esc_url($postmeta["audio_post_url"][0]) .'"]') ?>
							</div>
							<?php
							}
							?>	
							<?php if(has_post_format( 'video' , get_the_id())){ ?>
								<div class = "post-meta videoGallery">
							<?php } 
							
							else {?>
								<div class = "post-meta">
							<?php } 		
								$day = get_the_time('d');
								$month= get_the_time('m');
								$year= get_the_time('Y');
								?>
								<?php echo '<a class="post-meta-time" href="'.get_day_link( $year, $month, $day ).'">'; ?><?php the_time('F j, Y') ?></a>
								</div>	
						</div>
					<?php } else {?>
						<?php
						global $post;
						$post_subtitrare = get_post( get_the_id() );
						$content = $post_subtitrare->post_content;
						$pattern = get_shortcode_regex();
						preg_match( "/$pattern/s", $content, $match );
						if( isset( $match[2] ) && ( "gallery" == $match[2] ) ) {
							$atts = shortcode_parse_atts( $match[3] );
							$attachments = isset( $atts['ids'] ) ? explode( ',', $atts['ids'] ) : get_children( 'post_type=attachment&post_mime_type=image&post_parent=' . get_the_id() .'&order=ASC&orderby=menu_order ID' );
						}
						if ($attachments) {?>
						<div class="gallery-single">
						<?php
							foreach ($attachments as $attachment) {
								$title = '';
								//echo apply_filters('the_title', $attachment->post_title);
								$image =  wp_get_attachment_image_src( $attachment, 'gallery' ); 	 
								$imagefull =  wp_get_attachment_image_src( $attachment, 'full' ); 	 ?>
									<div class="image-gallery">
										<a href="<?php echo esc_url($imagefull[0]) ?>" rel="lightbox[single-gallery]" title="<?php echo get_post_meta($attachment, '_wp_attachment_image_alt', true) ?>"><div class = "over"></div>
											<img src="<?php echo esc_url($image[0]) ?>" alt="<?php echo get_post_meta($attachment, '_wp_attachment_image_alt', true) ?>"/>			
										</a>	
									</div>			
									<?php } ?>
						</div>
						<div class="bottomborder"></div>
						<?php } ?>
							<div class = "post-meta videoGallery">
								<?php 
								$day = get_the_time('d');
								$month= get_the_time('m');
								$year= get_the_time('Y');
								?>
								<?php echo '<a class="post-meta-time" href="'.get_day_link( $year, $month, $day ).'">'; ?><?php the_time('F j, Y') ?></a><?php echo '<a class="post-meta-author" href="'.get_the_author_meta('user_url').'">'.__('Written by: ','pmc-themes') . get_the_author().'</a>' ?>  <a class="post-meta-comments" href="#commentform"><?php comments_number(); ?></a>
								</div>	
					<?php }  ?>
					<div class="sentry">
						<?php if ( has_post_format( 'video' , get_the_id())) {?>
							<div><?php the_content(); ?></div>
						<?php
						}
					    if ( has_post_format( 'audio' , get_the_id())) { ?>
							<div><?php the_content(); ?></div>
						<?php
						}						
						if(has_post_format( 'gallery' , get_the_id())){?>
							<div class="gallery-content"><?php the_content(); 	?></div>
						<?php } 
						if( !get_post_format()){?> 
						    <?php add_filter('the_content', 'pmc_addlightbox'); ?>
							<div><?php the_content(); ?></div>		
						<?php } ?>
						<div class="post-page-links"><?php wp_link_pages(); ?></div>
						<div class="singleBorder"></div>
					</div>
				</div>
				
				<?php if(has_tag()) { ?>
					<div class="tags"><i class = "fa fa-tags"></i><?php the_tags('',', ',''); ?></div>	
				<?php } ?>
				
				<div class="share-post">
					<div class="share-post-title">
						<h3><?php _e('Share this post','pmc-themes') ?></h3>
					</div>
					<div class="share-post-icon">
						<div class="socialsingle">&nbsp;</div>	
					</div>
				</div>
				
				<div class = "author-info-wrap">
				<div class="blogAuthor">
					
				</div>
					<div class="authorBlogName">	
						
					</div>
					<div class = "bibliographical-info"></div>
				</div>
				
			</div>						
			
		</div>	
		
		<?php
		$posttags = wp_get_post_tags(get_the_id(), array( 'fields' => 'ids' ));
		$query_custom = new WP_Query(
			array( "tag__in" => $posttags,
				   "orderby" => 'rand',
				   "showposts" => 3,
				   "post__not_in" => array(get_the_id())
			) );
		if ($query_custom->have_posts()) : ?>
			<div class="titleborderOut">
				<div class="titleborder"></div>
			</div>
			<div class="relatedtitle">
				<h4><?php  _e('Related Posts','pmc-themes'); ?></h4>
			</div>
			<div class="related">	
			
			<?php
			$count = 0;
			while ($query_custom->have_posts()) : $query_custom->the_post(); 
				if(pmc_getImage(get_the_id(), 'related') !=''){
					$image_related = pmc_getImage(get_the_id(), 'related');
				}
				else{
					$image_related = '<img src="http://placehold.it/370x250">';
				}
				if($count != 2){ ?>
					<div class="one_third">
				<?php } else { ?>
					<div class="one_third last">
				<?php } ?>
						<div class="image"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php echo $image_related ?></a></div>
						<h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
						<?php
						$day = get_the_time('d');
						$month= get_the_time('m');
						$year= get_the_time('Y');
						?>
						<?php echo '<a class="post-meta-time" href="'.get_day_link( $year, $month, $day ).'">'; ?><?php the_time('F j, Y') ?></a>						
					</div>
						
				<?php 
				$count++;
			endwhile; ?>
			</div>
		<?php endif;
		wp_reset_postdata(); 
		
		?>	
	
	
	<?php comments_template(); ?>
	<div class = "post-navigation">
		<?php next_post_link('%link', '<div class="link-title-previous"><span>&#171; '.__('Previous post','pmc-themes').'</span><div class="prev-post-title">%title</div></div>' ,false,''); ?> 
		<?php previous_post_link('%link','<div class="link-title-next"><span>'.__('Next post','pmc-themes').' &#187;</span><div class="next-post-title">%title</div></div>',false,''); ?> 
	</div>		
	<?php endwhile; else: ?>
					
		<?php get_template_part('404'); ?>
	<?php endif; ?>
	</div>
	<?php if(!isset($pmc_data['use_fullwidth'])) { ?>
		<div class="sidebar">	
			<?php dynamic_sidebar( 'sidebar' ); ?>
		</div>
	<?php } ?>
</div>
</div>
<?php get_footer(); ?>
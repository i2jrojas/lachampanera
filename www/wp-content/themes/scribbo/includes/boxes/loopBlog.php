<?php global $pmc_data; ?>
	<div class="entry">
		<div class = "meta">
			<div class="topLeftBlog">	
				
				<div class = "post-meta">
					<?php 
					$day = get_the_time('d');
					$month= get_the_time('m');
					$year= get_the_time('Y');
					?>
					<?php echo '<a class="post-meta-time" href="'.get_day_link( $year, $month, $day ).'">'; ?><?php the_time('F j, Y') ?></a>
				</div>	
			</div>
			
			<div class="blogContent">
				<div class="blogcontent"><?php the_excerpt() ?></div>
				<a class="blogmore" href="<?php the_permalink() ?>"><?php _e('Continue reading','pmc-themes'); ?> </a>
			</div>
		</div>		
	</div>
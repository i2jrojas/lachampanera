<?php
global $pmc_data
?>
<div class="totop"><div class="gototop"><div class="arrowgototop"></div></div></div>
<!-- footer-->

<footer>
	<?php
	if(isset($pmc_data['use_block3']) && isset($pmc_data['block3_username']) && function_exists('APTFINbyTAP_admin_widget_script')){ ?>
		<div class="block3">
			<a href="<?php echo esc_url($pmc_data['block3_url']) ?>" target="_blank">
				<h5 class="block3-instagram-title">OUR INSTAGRAM FEED</h5>
				<div class="block3-instagram-username"><?php echo esc_attr($pmc_data['block3_username']) ?></div>
			</a>
		</div>
		<?php
		echo do_shortcode('[alpine-phototile-for-instagram id=517 user="'.esc_attr($pmc_data['block3_username']).'" src="user_recent" imgl="instagram" style="wall" row="6" size="M" num="6" align="center" max="100" nocredit="1"]');
	}
	?>
	<div id="footer">
	
	
	<div id="footerinside">
	<!--footer widgets-->
		<div class="footer_widget">
			<div class="footer_widget1">
				<?php dynamic_sidebar( 'footer1' ); ?>			
			</div>	
			<div class="footer_widget2">	
				<?php dynamic_sidebar( 'footer2' ); ?>
			</div>	
			<div class="footer_widget3">	
				<?php dynamic_sidebar( 'footer3' ); ?>
			</div>
		</div>
	</div>		
	</div>
	<!-- footer bar at the bootom-->
	<div id="footerbwrap">
		<div id="footerb">
			<div class="lowerfooter">
			<div class="copyright">	
            	<div class ="center-footer-content">
                    
                    <div style="margin:auto;width:230px;">
                    <div style="float:left;">
                        © La Champanera <?php echo date('Y'); ?> &nbsp; |
                    </div>
                    <div style="float:left;">
                   <a href="http://morganmedia.es" target="_blank" title="Posicionamiento web" ><img  src="http://morganmedia.es/repository/logo-morgan-media-footer-f.png" alt="Posicionamiento web" /></a></div>
                    </div>
                
                </div>
			</div>
			</div>
		</div>
	</div>	
</footer>	
<?php wp_footer();  ?>
</body>
</html>

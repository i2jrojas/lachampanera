<?php global $pmc_data; 
$postmeta = get_post_custom(get_the_id()); 
if(isset($postmeta["link_post_url"][0])){
	$link = $postmeta["link_post_url"][0];
} else {
	$link = "#";
}
?>
	<div class="entry">
		<div class = "meta">
			<div class="topLeftBlog">	
				
				<div class = "post-meta">
					<?php echo '<a class="post-meta-time" href="'.get_day_link( $year, $month, $day ).'">'; ?><?php the_time('F j, Y') ?></a>
				</div>	
			</div>
			<div class="blogContent">
				<div class="blogcontent"><?php the_excerpt(); ?> </div>
				<a class="blogmore" href="<?php echo esc_url($link)  ?>"><?php _e('Check out this link','pmc-themes'); ?> </a>
			</div>
		</div>		
	</div>	